export enum ResponseCode {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NO_CONTENT = 204,
    NOT_MODIFIED = 304,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404
};

export enum Level {
    Newbie = 0,
    // Novice,
    Rookie,
    // Beginner,
    // Talented,
    Skilled,
    // Intermediate,
    // Skillful,
    // Seasoned,
    // Proficient,
    // Experienced,
    // Advanced,
    // Senior,
    Expert
};

export enum WordState {
    wrong = -1,
    untreated = 0,
    right = 1,
}

export class ResponseEventEmiter {
    event: any;
    next: boolean;
    constructor (event, next) {
        this.event = event;
        this.next = next;
    }
}