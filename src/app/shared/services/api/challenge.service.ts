import {v4 as uuidv4} from 'uuid';

export class Challenge {
    public id: string;
    public nickname: string;
    public minutes: number;
    public secondscosumed: number;
    public startOn: string;
    public endOn: string;
    public textchallenge: string;
    public challengedone: string;
     
    constructor() {
        this.id = uuidv4();
        this.nickname = '';
        this.secondscosumed = 0;
        this.textchallenge = '';
        this.challengedone = '';
    }
}