import { PipeTransform, Injectable, Pipe } from '@angular/core';

@Pipe({
    name: 'currencyar'
})
@Injectable()
export class CurrencyAR implements PipeTransform {
    transform(value: number): string {
        const currencySign = '$ ';

        let result = value.toString().replace(',', '?').replace('.', ',').replace('?', '.');
        const parts = result.toString().split(',');
        if (parts.length === 1) {
            result += ',00';
        } else if (parts[1].length < 2) {
            result += '0';
        }
        return currencySign + result;
    }
}
