import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BlankComponent } from './layouts/blank/blank.component';
import { AuthGuard } from './shared/guard/auth.guard';

export const routes: Routes = [
{
    path: '',
    component: BlankComponent,
    children: [
        {
            path: '',
            loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengeModule)
        }
    ]
},
{
    path: '**',
    redirectTo: '404'
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }), NgbModule],
    exports: [RouterModule]
})
export class AppRoutingModule { }

