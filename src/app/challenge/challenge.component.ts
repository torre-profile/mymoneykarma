import { 
    AfterContentChecked,
    ChangeDetectorRef,
    Component, OnInit 
} from '@angular/core';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';
import { Challenge, LocalService, ResponseEventEmiter } from '../shared';
import { environment } from '../../environments/environment';

@Component({
	selector: 'app-challenge',
	styleUrls: ['./challenge.component.scss'],
	templateUrl: './challenge.component.html'
})
export class ChallengeComponent implements OnInit, AfterContentChecked  {
    readonly MODULE = "Challenge";

	readonly VERSION = 'version';
    readonly EXPIRATION_DATE = 'expiration';
    readonly CHALLENGE_DATA = 'challengedata';
    readonly SESSION_TIMEOUT = 15;

	step: number;
	challenge: Challenge;

	public constructor(
        private cdr: ChangeDetectorRef,
        private modal: Modal,
        private localService: LocalService) {
            this.challenge = new Challenge();
            this.step = 1;
	 }

	ngOnInit(): void {
        const date = new Date(this.localService.getJsonValue(this.EXPIRATION_DATE));
        const minutes = Math.abs(Date.now() - date.getTime()) / (1000 * 60) % 60;
        if(minutes > this.SESSION_TIMEOUT) {
            this.localService.clearToken();
            return;
        } 
        
        if(environment.VERSION != this.localService.getJsonValue(this.VERSION)){
            this.localService.clearToken();
            return;
		}
		
		this.challenge = this.localService.getJsonValue(this.CHALLENGE_DATA);
	}

    ngAfterContentChecked(): void {
        this.cdr.detectChanges();
    }  
    
    handleChallengeUpdated(event: ResponseEventEmiter) {
        const date = new Date();
        this.localService.setJsonValue(this.VERSION, environment.VERSION);
        this.localService.setJsonValue(this.EXPIRATION_DATE, date);
        this.localService.setJsonValue(this.CHALLENGE_DATA, this.challenge);
        if(event.next) {
            this.step++;
        }
    }

    handleError(error) {
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.MODULE)
            .keyboard(27)
            .body(error)
            .okBtn('Aceptar')
            .open();
    }
}
