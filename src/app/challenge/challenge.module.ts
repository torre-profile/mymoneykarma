import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { ChallengeComponent } from './challenge.component';
import { ChallengeRoutingModule } from './challenge-routing.module';
import { 
    StartComponent,
    TypingComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        ReactiveFormsModule,
        ChallengeRoutingModule
    ],
    declarations: [
        ChallengeComponent,
        StartComponent,
        TypingComponent
    ],
    exports: [ChallengeComponent]
})
export class ChallengeModule { }
