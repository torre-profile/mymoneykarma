import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChallengeComponent } from './challenge.component';

const routes: Routes = [
    {
        path: '',
        component: ChallengeComponent,
        data: {
            title: 'Challenge'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChallengeRoutingModule { }
