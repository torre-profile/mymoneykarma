import {
    Component, OnInit, 
    Input, Output, 
    EventEmitter, SimpleChange, OnChanges, AfterViewChecked,
    ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

import {
    Challenge, LocalService, ResponseEventEmiter, WordState } from '../../../shared';

import DictionaryJson from 'assets/mock-jsons/dictionary.json';

interface Dictionary {
    leter: string;
    words: string;
}

@Component({
    selector: 'app-start',
    styleUrls: ['./start.component.scss'],
    templateUrl: './start.component.html',
    providers: [
        FormBuilder
    ]
})
export class StartComponent implements OnInit, OnChanges, AfterContentChecked {
    readonly MODULE = "Start";

    readonly CHALLENGE_DATA = 'challengedata';

    @Input() challenge: Challenge = new Challenge();
    @Output() challengeUpdated = new EventEmitter()

    formEntity = this.formBuilder.group({
        minutes: new FormControl(null, Validators.required),
        textchallenge: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(1024*100)])),
        dochallenge: new FormControl('', Validators.maxLength(500))
    });
    
    get nickname() {return this.formEntity.get('nickname')};
    get minutes() {return this.formEntity.get('minutes')};
    get textchallenge() {return this.formEntity.get('textchallenge')};
    get dochallenge() {return this.formEntity.get('dochallenge')};

    custom = 0;

    dictionary: Dictionary[] = DictionaryJson;

    challengeAccurancy = new Array<any>();

    playing = false;

    public constructor(
        private changeDetector: ChangeDetectorRef,
        private modal: Modal,
        private localService: LocalService,
        private formBuilder: FormBuilder) {
    }

    public ngOnInit(): void {
    }
    
    ngAfterContentChecked() : void {
        this.changeDetector.detectChanges();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['challenge'].currentValue !== null &&
            changes['challenge'].currentValue !== undefined) {
            this.challenge = changes['challenge'].currentValue;
            this.formEntity.patchValue({
                    nickname: this.challenge.nickname,
                    minutes: (this.challenge.minutes != undefined) ? (this.challenge.minutes + '') : undefined,
                    textchallenge: this.challenge.textchallenge
                });
            this.challengeUpdated.emit(new ResponseEventEmiter(this.formEntity.valid, false));
        }
    }

    setMinutes(minutes) {
        const randText = this.getRandomTest(minutes).join(' ');
        this.formEntity.patchValue({
            minutes: minutes + '',
            textchallenge: randText
        });

        this.custom = (minutes == 0) ? 1 : 0;
    }

    setCustomMinutes(minutes) {
        const randText = this.getRandomTest(minutes.target.value);
        const text = randText.join(' ');
        this.formEntity.patchValue({
            minutes: minutes.target.value,
            textchallenge: text
        });
    }

    getRandomTest(minutes) {
        const averageWordsPerMinute = 100;
        const totalWords = minutes * averageWordsPerMinute;
        let wordsList = new Array<string>();
        for(let i = 0; i < totalWords; i++) {
            const letterIndex=this.randomInteger(0,25);
            const words = this.dictionary[letterIndex].words.split(' ');
            const index = this.randomInteger(0, words.length - 1);
            wordsList.push(words[index]);
        }
        return wordsList;
    }

    onTextChallenge(textchallenge) {
        this.challenge.textchallenge = textchallenge;
    }

    detectWordEnd() {
        this.challenge.challengedone += this.dochallenge.value;
    }

    randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    onStart() {
        this.minutes.disable();
        this.challenge.textchallenge = this.textchallenge.value;
        const textchallenge = this.challenge.textchallenge;
        textchallenge
            .split(' ')
            .forEach(word => {
                    this.challengeAccurancy.push({
                        "word": word,
                        "state": WordState.untreated
                    })
                });
        this.challenge.minutes = this.minutes.value;
        this.playing = true;

        // this.challengeUpdated.emit(new ResponseEventEmiter(this.formEntity.valid, true));
    }
}
