import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';
import TestsJson from 'assets/mock-jsons/tests.json';

interface Test {
    id: number;
    level: string;
    test: string;
}

import {
    Challenge,
    LocalService } from '../../../shared';

@Component({
    selector: 'app-typing',
    templateUrl: './typing.component.html',
    styleUrls: ['./typing.component.scss'],
    providers: [
        FormBuilder
    ]
})

export class TypingComponent implements OnInit {
    readonly CHALLENGE_DATA = 'challengedata';

   challenge: Challenge = new Challenge();
    @Output() challengeUpdated = new EventEmitter();

    tests: Test[] = TestsJson;

    formEntity = this.formBuilder.group({
        nickname: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])),
    });
    
    get nickname() {return this.formEntity.get('nickname')}

    public constructor(
        public router: Router, 
        private route: ActivatedRoute,
        private modal: Modal,
        private formBuilder: FormBuilder,
        private localService: LocalService) {
    }

    public ngOnInit(): void {
        this.getData();
    }

    public getData() {
        this.challenge = this.localService.getJsonValue(this.CHALLENGE_DATA);
        if(this.challenge.secondscosumed == 0) {
            this.startChallenge();
        }
    }

    startChallenge() {
    }
}
