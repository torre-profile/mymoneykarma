import {
    Component, OnInit
} from '@angular/core';
import { Observable } from 'rxjs';
import { 
    LocalService,
    TorrePersonService
    } from '../shared';
import { environment } from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

@Component({
    selector: 'app-profile',
    styleUrls: ['./profile.component.scss'],
    templateUrl: './profile.component.html',
    providers: [
        TorrePersonService
    ]
})
export class ProfileComponent implements OnInit {
    readonly MODULE = "Profile";

    readonly VERSION = 'version';
    readonly EXPIRATION_DATE = 'expiration';
    readonly COTIZACION_DATA = 'persondata';
    readonly SESSION_TIMEOUT = 15;

    option = 0;

    selectedEntity;

    public constructor(
        private route: ActivatedRoute,
        private modal: Modal,
        private tpersonService: TorrePersonService,
        private localService: LocalService,
        ) {
    }

    ngOnInit(): void {
        const date = new Date(this.localService.getJsonValue(this.EXPIRATION_DATE));
        const minutes = Math.abs(Date.now() - date.getTime()) / (1000 * 60) % 60;
        // if(minutes > this.SESSION_TIMEOUT) {
        //     this.localService.clearToken();
        //     return;
        // }
        
        // if(environment.VERSION != this.localService.getJsonValue(this.VERSION)){
        //     this.localService.clearToken();
        //     return;
        // }

        const routeParams = this.route.snapshot.paramMap;
        const publicID = routeParams.get('publicId');
        this.getData(publicID);
        
        // this.cotizacion = this.localService.getJsonValue(this.COTIZACION_DATA);
    }

    getData(publicID) {
        this.tpersonService
            .getByID(publicID)
            .subscribe({
                next: this.setEntity.bind(this),
                error: this.handleError.bind(this)
            })
            
    }

    setEntity(person) {
        this.selectedEntity = person;
        this.localService.setJsonValue(this.COTIZACION_DATA, person);
    }
    
    onSetOption(option) {
        this.option = option;
    }
    
    handleCotizacionUpdated(enabled) {
        const date = new Date();
        this.localService.setJsonValue(this.VERSION, environment.VERSION);
        this.localService.setJsonValue(this.EXPIRATION_DATE, date);
        // this.localService.setJsonValue(this.COTIZACION_DATA, this.cotizacion);
    }

    handleError(error) {
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.MODULE)
            .keyboard(27)
            .body(error)
            .okBtn('Aceptar')
            .open();
    }
}
