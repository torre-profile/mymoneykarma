import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
    LocalService,
    TWUtils } from '../../../shared';

@Component({
    selector: 'app-skills',
    templateUrl: './skills.component.html',
    styleUrls: ['./skills.component.scss'],
    providers: []
})

export class SkillsComponent implements OnInit {
    readonly COTIZACION_DATA = 'persondata';

    public selectedEntity;
    public strengths;
    public masterSkills;
    public expertSkills;
    public proficientSkills;
    public noviceSkills;
    public inexperiencedSkills;

    public constructor(
        public router: Router, 
        private route: ActivatedRoute,
        private localService: LocalService) {
    }

    public ngOnInit(): void {
        this.getData();
    }

    public getData() {
        this.selectedEntity = this.localService.getJsonValue(this.COTIZACION_DATA);
        if(this.selectedEntity) {
            this.masterSkills = this.selectedEntity.strengths.filter(skill => skill.proficiency=='master');
            this.expertSkills = this.selectedEntity.strengths.filter(skill => skill.proficiency=='expert');
            this.proficientSkills = this.selectedEntity.strengths.filter(skill => skill.proficiency=='proficient');
            this.noviceSkills =  this.selectedEntity.strengths.filter(skill => skill.proficiency=='novice');
            this.inexperiencedSkills = this.selectedEntity.strengths.filter(skill => skill.proficiency=='no-experience-interested');
            // this.strengths = this.selectedEntity.strengths;
        }
    }
}
