import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
    LocalService,
    TWUtils } from '../../../shared';

@Component({
    selector: 'app-personal',
    templateUrl: './personal.component.html',
    styleUrls: ['./personal.component.scss'],
    providers: []
})

export class PersonalComponent implements OnInit {
    readonly COTIZACION_DATA = 'persondata';

    public selectedEntity;

    public constructor(
        public router: Router, 
        private route: ActivatedRoute,
        private localService: LocalService) {
    }

    public ngOnInit(): void {
        this.getData();
    }

    public getData() {
        this.selectedEntity = this.localService.getJsonValue(this.COTIZACION_DATA);
    }
}
